## Chuck Noris Jokes

A react application using the [Chuck Noris API](http://www.icndb.com/api/).

In the project directory, you can run:

`npm i && npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


## Next
- Add pagination
- Unit test