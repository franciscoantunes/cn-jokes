import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";
import Joke from './Joke';

const Home = () => {
    const [joke, setJoke] = useState('');
    const getJoke = async () => {
        await fetch('http://api.icndb.com/jokes/random?exclude=[explicit]')
                    .then(data => data.json())
                    .then(data => setJoke(data.value.joke))
                    .catch(error => console.error(error));
    };

    return (
        <div>
            <Button variant="contained" color="primary" onClick={ getJoke }>
                Make my day happier <span role="img" aria-label="laugh emoji">😂</span> and load a good Chuck Noris joke <span role="img" aria-label="karate emoji">🙅‍♂️</span>
            </Button>
            <Joke joke={joke}></Joke>
            { joke && <p>Didn't find it funny? <Link to="/search">Try changing the name</Link></p> }
        </div>
    );
};

export default Home;
