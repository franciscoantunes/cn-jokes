import React from 'react';

const Joke = ({ joke }) => {
    return (
        <div>
            {joke && <p className="joke-quote">{ joke }</p>}
        </div>
    );
};

export default Joke;
