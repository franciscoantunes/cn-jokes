import React, { useState } from 'react';
import { Link } from "react-router-dom";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Joke from './Joke';

const Search = () => {
    const [query, setQuery] = useState('');
    const [validation, setValidation] = useState(false);
    const [joke, setJoke] = useState('');

    const updateQuery = evt => {
        setQuery(evt.target.value);
    };

    const customJoke = async () => {
        if (query) {
            const fullName = query.split(' '); // This will ignore more than 2 names
            const firstName = fullName[0];
            const lastName = fullName[1] || 'Noris';

            await fetch(`http://api.icndb.com/jokes/random?firstName=${firstName}&lastName=${lastName}&exclude=[explicit]`)
                        .then(data => data.json())
                        .then(data => setJoke(data.value.joke))
                        .catch(error => console.error(error));
            setValidation(true);
        } else {
            setValidation(false);
        }
    };

    return (
        <div>
            <p>Add the first and last name below to make a personilised joke:</p>
            <TextField label="Full name" value={query} onChange={updateQuery} size="small" />
            <Button variant="contained" color="primary" onClick={ customJoke }>
                Replace Chuck Noris
            </Button>
            { validation ? <Joke joke={joke}></Joke> : <p>Name field is required</p>}
            { joke && <p>Still not finding this funny? <Link to="/">Try a random joke then</Link></p>}
        </div>
    );
};

export default Search;
