import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { Container } from '@material-ui/core';
import Home from './Home';
import Search from './Search';

import './App.css';

const App = () => {
  return (
    <div className="App">
      <Container>
        <h1>Chuck Noris Jokes</h1>

        <Router>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/search" component={Search} />
          </Switch>
        </Router>
      </Container>
    </div>
  )
};

export default App;
